#include <iostream>
using namespace std;

void size_convert(float [],int ); 
float area(float ,float ,float );  
float price_brick(int ,int ,float );  
float discount10(float ); 
float checkbefore_dc(int ,int ,int ,float ,float ,float ); 
float dt(float ); 

int main()
{
	int member, loop=0, check_brick_type, amount, buyclay_check=0, buygold_check=0, buyu_check=0, amountNow=0; 
	float size_inch[3]={}, price_clay=0, price_gold=0, price_u=0, area_result=0, pre_price=0, distance, price_result=0, price_dt=0; 
	
	cout << "=========================================" <<endl; 
	cout << "       Welcome! to Ken's Brick Shop.     " <<endl; 
	cout << "=========================================" <<endl; 
	//ask are you member
	cout << "Is customer member ? (0=Yes or 1=No) : "; 
	cin >> member; 

	
	switch(member)
	{ 
	//Customer is member
		case 0 : 
				do
				{
					//ask type of brick
					cout << "Material of Brick (0=Clay , 1=Gold , 2=Unobtainium) : " <<endl;
					cin >> check_brick_type;
			
					//ask amount of brick form previous
					cout << "Amount of Brick(s) : " <<endl; 
					cin >> amount; 
			
					//Count amount of brick(s)
					amountNow = amountNow + amount; 
			
					//ask W L H
					cout << "Please input size of brick following by Width,Length,Height ex. 1 1 1 in inches : " << endl; 
					for(int j = 0 ; j < 3 ; j++)
					{
						cin >> size_inch[j]; 
					}
			
					//convert W L H to cen.
					size_convert(size_inch, 3); 
			
					//cal area
					area_result = area(size_inch[0], size_inch[1], size_inch[2]); 
			
					//Clay
					if(check_brick_type == 0)
					{ 
						price_clay = price_clay + price_brick(0, amount, area_result); 
						buyclay_check = 1; 
					}
					//Gold
					else if(check_brick_type == 1)
					{  
						price_gold=price_gold+price_brick(1, amount, area_result); 
						buygold_check = 1;
					}
					//Unobtainium 
					else if(check_brick_type == 2)
					{  
						price_u = price_u + price_brick(2, amount, area_result);  
						buyu_check = 1; 
					}

					//ask for another brick
					cout << endl;
					cout << "********************************READ ME**********************************" << endl; 
					cout << "IF YOU WANT TO ADD NEXT BRICK PLEASE INPUT 0 **BUT INPUT ANYELSE TO END**" << endl; 
					cout << "*************************************************************************" << endl; 
					cout << "Input : "; 
					cin >> loop; 
					}while(loop == 0); 

					//amount less than 3
					if(amountNow < 3)
					{
						//Calculate price before discount
						pre_price = price_clay + price_gold + price_u; 
			
						//Cal price after dc
						pre_price = discount10(pre_price); 
					}
					
					//amount more than or equal 3
					if(amountNow >= 3)
					{
						//Cal price after dc. cheapest brick
						pre_price = checkbefore_dc(buyclay_check, buygold_check, buyu_check, price_clay, price_gold, price_u);
					}
	
					cout <<endl;
		
					//ask distance
					cout << "Please input Distance(km.) for deliveried by Ken's Shop (0 = No Delivery) : ";
					cin >> distance;
		
					//Calculate discount 10% of distances price
					price_dt = 0.9 * dt(distance); 
		
					cout << endl;
		
					//Price Result
					price_result = pre_price + price_dt; 
		
					cout << "*****************************"<<endl;
					cout << "            Receipt         "<<endl; 
					cout << endl;
					cout << "Price of Brick(s) " << pre_price <<" B."<<endl; 
					cout << "Price of Distances " << price_dt <<" B."<<endl; 
					cout << "Price Result : "  << price_result <<" B."<<endl; 
					cout << endl;
					cout << "          THANK YOU :)       "<<endl; 
					cout << "*****************************"<<endl; break; 
		
		case 1 : 
				//Customer isn't member
				do
				{
					//ask type of brick
					cout << "Material of Brick (0=Clay , 1=Gold , 2=Unobtainium) : " <<endl; 
					cin >> check_brick_type; 
	
					//ask amount of brick form previous
					cout << "Amount of Brick(s) : " <<endl; 
					cin >> amount; 
			
					//ask W L H
					cout << "Please input size of brick following by Width,Length,Height ex. 1 1 1 in inches : " << endl; 
					for(int j = 0 ; j < 3 ; j++)
					{
						cin>> size_inch[j]; 
					}
			
					//convert W L H to cen.
					size_convert(size_inch, 3); 
			
					//cal area
					area_result=area(size_inch[0], size_inch[1], size_inch[2]); 
			
					//Cal price before add with price of distance
					pre_price=pre_price+price_brick(check_brick_type, amount, area_result);
			
					//ask for another brick
					cout << endl;
					cout << "********************************READ ME**********************************" << endl; 
					cout << "IF YOU WANT TO ADD NEXT BRICK PLEASE INPUT 0 **BUT INPUT ANYELSE TO END**" << endl; 
					cout << "*************************************************************************" << endl; 
					cout << "Input : "; 
					cin >> loop; 
					}while(loop == 0);
			
					//ask distance
					cout <<endl;
					cout << "Please input Distance(km.) for deliveried by Ken's Shop (0 = No Delivery) : ";
					cin >> distance; 
			
					//cal price dt
					price_dt = dt(distance); 
	
					cout << endl;
			
					//cal price result
					price_result = pre_price + price_dt;
	
					cout << "*****************************"<<endl; 
					cout << "            Receipt         "<<endl;
					cout << endl;
					cout << "Price of Brick(s) " << pre_price <<" B."<<endl; 
					cout << "Price of Distances " << price_dt <<" B."<<endl; 
					cout << "Price Result : "  << price_result <<" B."<<endl; 
					cout << endl;
					cout << "          THANK YOU :)       "<<endl; 
					cout << "*****************************"<<endl; break; 
					default :
						cout << "Wrong input , Try again!" ; break;
	}
	return 0; 
}

//function that convert inches to cm.
void size_convert(float inch[],int n)
{ 
	for(int i = 0 ; i < n ; i++)
	{
		inch[i] *= 2.54; 
	}
}

//function that calculate surface area. & s stand for size_inch[i]
float area(float s0, float s1, float s2)
{ 
	float result = (2*(s0*s1 )) + (2*(s1*s2)) + (2*(s0*s2)); 
	return result; 
}

//function that calculate each price of kind of brick
float price_brick(int type,int amount,float area)
{ 
	switch(type)
	{
		case 0 : 
		return amount*area*10; 
		case 1 : 
		return amount*area*30; 
		case 2 :
		return amount*area*90;
		default : ; 
	}
}

//function that calculate discount 10% of price
float discount10(float price)
{ 
	float sum = 0.9*price; 
	return sum; 
}

//function that check what kind of brick that customer buy and calculate discount price of brick
float checkbefore_dc(int bc,int bg,int bu,float pc,float pg,float pu)
{ 
	if(bc == 1 && bg == 0 && bu == 0)
	{
		return 0.9*((pc*0.8) + pg + pu);
	}
	else if(bc == 0 && bg == 1 && bu == 0)
	{
		return 0.9*((pg*0.8) + pc + pu); 
	}
	else if(bc == 0 && bg == 0 && bu == 1)
	{
		return 0.9*((pu*0.8) + pc + pg);
	}
	else if(bc == 1 && bg == 1 && bu == 0)
	{
		return 0.9*((pc*0.8) + pg + pu);
	}
	else if(bc == 1 && bg == 0 && bu == 1)
	{
		return 0.9*((pc*0.8) + pg + pu);
	}
	else if(bc==0 && bg == 1 && bu == 1)
	{
		return 0.9*((pg*0.8) + pc + pu); 
	}
	else if(bc ==0 && bg == 0 && bu == 0)
	{
		return 0; //return value
	}
	else if(bc == 1 && bg == 1 && bu == 1)
	{ 
		return 0.9*((pc*0.8) + pg + pu); 
	}
}

//function that calculate price of delivery 
float dt(float d)
{ 
	float sum=0; 
	if(d <= 5)
	{
		return 0;
	}
	else if (d > 5 && d <= 20)
	{
		sum = (d - 5)*100; 
		return sum; 
	}else if (d > 20) 
	{
		sum = ((d - 20)*150) + 1500; 
		return sum; 
	}
}
