#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void move(int rowColumn[][10],char ring,int level); //this function make user can move around him N,NE,E,SE,S,SW,W,NW and protect user wrong move
void boardgame(int rowColumn[][10]); //draw board game
void boardgame_origin(int rowColumn[][10]); //draw board game each new level
void checklastmove(int rowColumn[][10]); //check what locate at array that frodo is live
void checklevel(int rowColumn[][10],int X,int Y,char ring,int level); //check rider around Frodo and if user pass go to the next level
void rider(int rowColumn[][10],int level); //create Rider each level
void ridermove(int rowColumn[][10]);//this function make Rider can move around him N,NE,E,SE,S,SW,W,NW
void clear(int rowColumn[][10]);//Clear board for new level


int lastX , lastY; //Using global variable because if lastX,Y store new last index so apply to use whole program 
int lose; //if lose update to 1 that mean user lose but if it's 0 mean pass

int main(){
	
	int rowColumn[10][10] = {}; //grid 10x10
	
	cout << "Welcome! to the forest\n";	//Show message
	cout << "Unfornately T^T Frodo's stuck in the forest and need your some help to exit the forest BUT becareful that Rider.\n";
	cout << "The Frodo have a Ring. (Ring can make you invisible and freeze the rider)\n";
	cout << "But the ring can use only one times per level(s)." <<endl;
	cout << "Good Luck My Hero!!" 		<<endl;
	cout << "The Frodo can move around him\n";
	cout	<<"    \\     |    /      \n"
			<<"     7    8   9   \n"
			<<" <-- 4  Move  6 -->\n"
			<<"     1    2   3   \n"
			<<"    /     |    \\   " 	<< endl;
			
	int level =1;	
while(level<=10){ //For game can reach levels 10

	char ring='0'; //set variable value to old value
	clear(rowColumn); //clear board for new level
	rider(rowColumn,level); //create some Rider
	boardgame_origin(rowColumn); //To make new level Frodo will start at index 0,0
	
	while(lose!=1){ 
			
			checklastmove(rowColumn);	

				if(ring!='3'){ //Show message to ask for use the ring
						cout << "Use the ring ?? ( Y for Use , N for Not use) : ";
						cin >> ring;
					while(ring!='Y'&&ring!='y'&&ring!='N'&&ring!='n'&&ring!=3){ //if wrong input user can try again
						cout << "Wrong input,Try again! : ";
						cin >> ring;
					}
				}
		
				if(ring=='N'||ring=='n'){ //If not use show this message
						cout <<"You are not using the ring" <<endl;
				}
		
				if(ring=='Y'||ring=='y'){ //If use the ring freeze the Rider
								checklastmove(rowColumn); 
			
							if(lastX==9&&lastY==9){
								level=level+1;
								break;}
				
						cout << "You are invisible (Quickly move 1)" <<endl; //Told user are now invisible
								checklastmove(rowColumn);
								move(rowColumn,ring,level); //move 1 times
							if(lastX==9&&lastY==9){
								level=level+1;
								break;}
				
						cout << "You are invisible (Quickly move 2)" <<endl;
								checklastmove(rowColumn);
								move(rowColumn,ring,level);//move 2 times
							if(lastX==9&&lastY==9){
								level=level+1;
								break;}
				
						cout << "You are invisible (Quickly move 3)" <<endl;
								checklastmove(rowColumn);
								move(rowColumn,ring,level); //move 3 times
							if(lastX==9&&lastY==9){
								level=level+1;
								break;}
					ring='3'; //set value in variable's name ring for user can't use ring again
				}
		
				if(ring=='3'){ //Show this message if user used ring already
						cout <<endl;
						cout << "You can't use ring" <<endl;	
				}
	
		checklastmove(rowColumn);
		move(rowColumn,ring,level);
		ridermove(rowColumn); //change location of Rider
		
				if(lastX==9&&lastY==9){
					level=level+1;
					break;}
				
    }
    
    cout <<endl;
    
    if(lose==1){ //if user lose show this message
    	cout << "You lose!! Because The Rider behide you!!" <<endl;
    	break;
	}
	else{ //if user pass show this message
		cout << "!!!" <<endl;
    	cout << "Well you pass level " << level-1 << " Now level " << level << " Good luck my Hero!" <<endl; //Show level that user reach
	}	
}

 	if(level==1 && lose==1){ 
 		cout << "Rider laughing at you. "<< "[Your level(s) " << level << " ]"; //Show level that user reach
	}
	
	if(level>=2){
		cout << "Frodo thinks 'You are the GOD!' "<< "[Your level(s) " << level << " ]"; //Show level that user reach
	}
	
	return 0;
}

void boardgame(int rowColumn[][10]){ //Draw board game
	rowColumn[9][9]=3;
	cout << "==================================================\n";
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){
			cout <<"| ";
			if(rowColumn[i][j]==0)
				cout << "  "; //Show Blank
			else if(rowColumn[i][j]==1) 
				cout << "F "; //Show Frodo
			else if(rowColumn[i][j]==2) 
				cout << "R "; //Show Rider
			else if(rowColumn[9][9]==3)
				cout << "E "; //Show Exit
			cout <<"|";
		}
		cout << endl;
	}	
	cout << "==================================================\n";
}

void boardgame_origin(int rowColumn[][10]){ //Draw board game and make Frodo start at 0,0 in array
	rowColumn[0][0]=1;
	lastX=0;
	lastY=0;
	rowColumn[9][9]=3; //Exit always at location 9,9 in array
	cout << "==================================================\n";
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){
			cout <<"| ";
			if(rowColumn[i][j]==0)
				cout << "  "; 
			else if(rowColumn[i][j]==1) 
				cout << "F "; 
			else if(rowColumn[i][j]==2) 
				cout << "R ";
			else if(rowColumn[9][9]==3)
				cout << "E ";
			cout <<"|";
		}
		cout << endl;
	}	
	cout << "==================================================\n";
}


void checklastmove(int rowColumn[][10]){
	for(int i=0;i<=9;i++){
		for(int j=0;j<=9;j++){
			if(rowColumn[i][j]==1){
				lastX=i; //store last index X
				lastY=j; //store last index Y
				break; //stop loop if can find where Frodo live
			}
		}
 	}
}

void move(int rowColumn[][10],char ring,int level){
	int move;
	
	cout<< "Frodo's move : "; //promt users input their move
	cin>>move;
	
	while(move==5||move<1||move>9){ //if user wrong input let try again
		cout << "Wrong move,Try again!"<<endl;
		cout << "Frodo's move : ";
		cin>>move;
	}
	
if(move>=1&&move<=9&&move!=5){ //check move is correct
	
	if(move==1){ //move to SW
		if(lastX+1<10 && lastY-1>=0 && rowColumn[lastX+1][lastY-1]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX+1][lastY-1]=1;
			checklevel(rowColumn,lastX+1,lastY-1,ring,level); //check Rider around Frodo
			boardgame(rowColumn);
		lastX=lastX+1; //update value in variable for store the last move
		lastY=lastY-1; //	
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times) (If you use the ring, your invisible lost 1 times)"<<endl;
	}
	
	if(move==2){ //move to S
		if(lastX+1<10 && rowColumn[lastX+1][lastY]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX+1][lastY]=1;
			checklevel(rowColumn,lastX+1,lastY,ring,level);
			boardgame(rowColumn);
		lastX=lastX+1;
		lastY=lastY;
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times)"<<endl;	//Show this message for protect user not wrong move
	}
	
	if(move==3){ //move to SE
		if(lastX+1<10 && lastY+1<10 && rowColumn[lastX+1][lastY+1]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX+1][lastY+1]=1;
			checklevel(rowColumn,lastX+1,lastY+1,ring,level);
			boardgame(rowColumn);
		lastX=lastX+1;
		lastY=lastY+1;
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times)"<<endl;	
	}
	
	if(move==4){ //move to W
		if(lastY-1>=0 && rowColumn[lastX][lastY-1]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX][lastY-1]=1;
			checklevel(rowColumn,lastX,lastY-1,ring,level);
			boardgame(rowColumn);
		lastX=lastX;
		lastY=lastY-1;
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times)"<<endl;	
	}
	
	if(move==6){ //move to E
		if(lastY+1<10 && rowColumn[lastX][lastY+1]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX][lastY+1]=1;
			checklevel(rowColumn,lastX,lastY+1,ring,level);
			boardgame(rowColumn);
		lastX=lastX;
		lastY=lastY+1;
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times)"<<endl;	
	}
	
	if(move==7){ //move to NW
		if(lastX-1>=0 && lastY-1>=0 && rowColumn[lastX-1][lastY-1]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX-1][lastY-1]=1;
			checklevel(rowColumn,lastX-1,lastY-1,ring,level);
			boardgame(rowColumn);
		lastX=lastX-1;
		lastY=lastY-1;
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times)"<<endl;	
	}
	
	if(move==8){ //move to N
		if(lastX-1>=0 && rowColumn[lastX-1][lastY]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX-1][lastY]=1;
			checklevel(rowColumn,lastX-1,lastY,ring,level);
			boardgame(rowColumn);
		lastX=lastX-1;
		lastY=lastY;
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times)"<<endl;	
	}
	
	if(move==9){ //move to NE
		if(lastX-1>=0 && lastY+1<10 && rowColumn[lastX-1][lastY+1]!=2){
			rowColumn[lastX][lastY]=0;
			rowColumn[lastX-1][lastY+1]=1;
			checklevel(rowColumn,lastX-1,lastY+1,ring,level);
			boardgame(rowColumn);
		lastX=lastX-1;
		lastY=lastY+1;
		}else
			cout << "Frodo can't go this way, Try again! (If you use the ring, your invisible lost 1 times)"<<endl;	
	}
 }
}

void checklevel(int rowColumn[][10],int X,int Y,char ring,int level){ //Check pass condition
	if(ring!='Y' && ring!='y'){ //Not Using ring
	
		if(rowColumn[X+1][Y-1]==2 && (X+1>=0 && X+1<=9) && (Y-1>=0 && Y-1<=9) ) 
			lose=1; //If Rider behide Frodo 
		
		if(rowColumn[X+1][Y]==2 && (X+1>=0 && X+1<=9) )
			lose=1;
		
		if(rowColumn[X+1][Y+1]==2 && (X+1>=0 && X+1<=9) && (Y+1>=0 && Y+1<=9) )
			lose=1;
		
		if(rowColumn[X][Y-1]==2 && (Y-1>=0 && Y-1<=9) )
			lose=1;
		
		if(rowColumn[X][Y+1]==2 && (Y+1>=0 && Y+1<=9))
			lose=1;
			
		if(rowColumn[X-1][Y-1]==2 && (X-1>=0 && X-1<=9) && (Y-1>=0&&Y-1<=9) )
			lose=1;
		
		if(rowColumn[X-1][Y]==2 && (X-1>=0 && X-1<=9) )
			lose=1;
		
		if(rowColumn[X-1][Y+1]==2 && (X-1>=0 && X-1<=9) && (Y+1>=0 && Y+1<=9) )
			lose=1;
	}else cout << "You are using the ring!" <<endl; //Using ring
}

void rider(int rowColumn[][10],int level){ 
	int count=0;	
	
	level=level*3; //each level Rider will have level*3
	
srand(time(0)); //random
while(count <level){	
	
		int i=rand()%10; //random 0-9
		int j=rand()%10; //
		 
	if(i>=0 && i<=9 && j>=0 && j<=9 && rowColumn[i][j]!=1 && rowColumn[i][j]!=2 && rowColumn[i][j]!=3){
		rowColumn[i][j]=2; //Make location in array is Rider
		count++;
		}
	}
}

void ridermove(int rowColumn[][10]){
	srand(time(0));
	for(int i=0 ; i<10 ; i++){
		for(int j=0 ; j<10 ; j++){
			
			if(rowColumn[i][j]==2){
				
				int ridermove = rand()%9+1; //random 1-9
				
				while(ridermove==5)
					ridermove =rand()%9+1; //if random number equal to 5 random again.
				
				if(ridermove!=5){ //check number from random not equal to 5
						
						if(ridermove==1){ //rider move to SW
							if(i+1<=9 && j-1>=0 && j-1<=9){
								if(rowColumn[i+1][j-1]!=2 && rowColumn[i+1][j-1]==0){
									rowColumn[i][j]=0;
									rowColumn[i+1][j-1]=2;}}
						}
						
						if(ridermove==2){ //rider move to S
							if(i+1<=9){
								if(rowColumn[i+1][j]!=2 && rowColumn[i+1][j]==0){
									rowColumn[i][j]=0;
									rowColumn[i+1][j]=2;}}
						}
						
						if(ridermove==3){ //Rider move to SE
							if(i+1<=9 && j+1<=9){
								if(rowColumn[i+1][j+1]!=2 && rowColumn[i+1][j+1]==0){
									rowColumn[i][j]=0;
									rowColumn[i+1][j+1]=2;}}
						}
	
						if(ridermove==4){ //Rider move to W
							if(j-1>=0 && j-1<=9){
								if(rowColumn[i][j-1]!=2 && rowColumn[i][j-1]==0){
									rowColumn[i][j]=0;
									rowColumn[i][j-1]=2;}}
						}
	
						if(ridermove==6){ //Rider move to E
							if(j+1<=9){
								if(rowColumn[i][j+1]!=2 && rowColumn[i][j+1]==0){
									rowColumn[i][j]=0;
									rowColumn[i][j+1]=2;}}
						}
						
						if(ridermove==7){ //Rider move to NW
							if(i-1>=0 && i-1<=9 && j-1>=0 && j-1<=9){
								if(rowColumn[i-1][j-1]!=2 && rowColumn[i-1][j-1]==0){
									rowColumn[i][j]=0;
									rowColumn[i-1][j-1]=2;}}
						}
						
						if(ridermove==8){ //Rider move to N
							if(i-1>=0 && i-1<=9){
								if(rowColumn[i-1][j]!=2 && rowColumn[i-1][j]==0){
									rowColumn[i][j]=0;
									rowColumn[i-1][j]=2;}}
						}
	
						if(ridermove==9){ //Rider move to NE
							if(i-1>=0 && i-1<=9 && j+1<=9){
								if(rowColumn[i-1][j+1]!=2 && rowColumn[i-1][j+1]==0){
									rowColumn[i][j]=0;
									rowColumn[i-1][j+1]=2;}}
						}
				}
			}
		}
 	}
}

void clear(int rowColumn[][10]){ //Clear old Rider
	for(int i=0;i<=9;i++){
		for(int j=0;j<=9;j++){
			if(rowColumn[i][j]==2)
				rowColumn[i][j]=0;
		}
	}
}
