#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <iomanip>
using namespace std;

void num(vector<int>& number); //Random value of numbers and random number of sizes 
void sumnum(vector<int> number); //Sum all numbers
void maximum(vector<int> number); //Find maximum
void minimum(vector<int> number); //Find minimum
void selectionSort(vector<int>& number); //Sorted numbers from the lowest to the highest
void swap(int* indexmin,int* i); //Swap value in vector index
void numsort(vector<int> number); //Show numbers after sorted
void mean(vector<int> number); //Find mean 
void median(vector<int> number); //Find median
void mode(vector<int> number); //Find mode
void evennum(vector<int> number); //Find number of even numbers
void oddnum(vector<int> number); //Find number of odd numbers

int main(){
	
	vector<int> number; //Declear the vector
	
	num(number);
	selectionSort(number);
	sumnum(number);
	maximum(number);
	minimum(number);
	mean(number);
	median(number);
	mode(number);
	evennum(number);
	oddnum(number);
	numsort(number);
	
return 0;
}

void num(vector<int>& number) //O(n^2)
{
	int count = 0,num;
	
	srand(time(0)); //For random always not one times
		while(count <= rand()%100+50) //For random number of sizes between 50-150
		{
				num = rand()%101; //For random value numbers between 0-100
				number.push_back(num); //store value in vector
			count ++; 
		}
}

void sumnum(vector<int> number) //O(n)
{
	int sum = 0;
	
	cout << "\n1. The number of elements. : " << number.size() << endl; 
	
	for(int i = 0 ; i <= number.size()-1 ; i++)
	{
		sum = sum + number.at(i); 
	}
	
	cout << "\n2. The sum of all elements. : " << sum << endl;
}

void maximum(vector<int> number) //O(1)
{
	cout << "\n3. The highest value. : " << number.at(number.size()-1) << endl; 
}

void minimum(vector<int> number) //O(1)
{
	cout << "\n4. The lowest value. : " << number.at(0) << endl;
}

void selectionSort(vector<int>& number) //O(n^2)
{
	int indexmin;
	
	for (int i = 0 ; i <= number.size()-1 ; i++)
	{
		indexmin = i;
		
		for (int j = i+1 ; j <= number.size()-1 ; j++)
		{
			if(number.at(j) < number.at(indexmin)) 
					indexmin = j; 
		}
		swap(&number.at(indexmin) , &number.at(i)); //Swap value
	}
}

void swap(int *indexmin, int *i) //O(1)
{
	int hold = *indexmin; 
	*indexmin = *i;
	*i = hold;
}

void numsort(vector<int> number) //O(n)
{
	cout << "\n10. The values output in order from lowest to highest. \n";
	
		for(int i = 0 ; i <= number.size()-1 ; i++)
		{
			cout << setw(5) << number.at(i); 
			
			if(i%10==9)
			cout <<endl;
		}
}

void mean(vector<int> number) //O(n)
{
	float sum = 0;
	
	for(int i = 0 ; i <= number.size()-1 ; i++)
	{
		sum = sum + number.at(i);
	}
	
	cout <<"\n5. The mean value. : " << sum/number.size() <<endl;
}

void median(vector<int> number) //O(1)
{
	float value;
	int index = number.size()-1;
	float sum = index/2;
	
	value = number.at(sum);
	if(number.size()%2==0)
		if((number.at(sum)%2==1&&number.at(sum+1)%2==0)||(number.at(sum)%2==0&&number.at(sum+1)%2==1)) //If number of sizes are equal to even number need to calculate by (middle value + next one )/2 for find the median  
			value = ((number.at(sum)+number.at(sum+1))/2) + 0.5;
		else
			value = (number.at(sum)+number.at(sum+1))/2;
	
	cout <<"\n6. The median value. : " << value <<endl;
}

void mode(vector<int> number) //O(n^2)
{
	int freq[101]={};
	int max,mode=0;
	
	for(int i = 0 ; i <= number.size()-1 ; i++)
	{
		freq[number.at(i)]++;
	}
	
	max = freq[0];
	
	for(int i = 0 ; i <= 100; i++)
	{
		if(freq[i] > max)
		{
			max = freq[i];
			mode = i; //use mode = i for store value of number not store frequency of number
		}
	}
	cout << "\n7. The mode value. : " << mode <<endl;
}

void evennum(vector<int> number) //O(n)
{
	int count = 0;
	cout << "\n8. The number of even numbers. : ";
	
	for(int i = 0 ; i <= number.size()-1 ; i++)
	{
		if(number.at(i)%2==0) 
			count++;
	}
	cout << count;
	cout << endl;
}

void oddnum(vector<int> number) //O(n)
{
	int count = 0;
	cout << "\n9. The number of odd numbers. : ";
	
	for(int i = 0 ; i <= number.size()-1 ; i++)
	{
		if(number.at(i)%2==1)
		 count++;
	}
	cout << count;
	cout << endl;
}
