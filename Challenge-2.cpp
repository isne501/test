#include <iostream>
#include <ctime>
using namespace std;

void Table(); //show table
void UpdateTable(int arr[][7]); //update table
void player(int arr[][7]);//player turn
void player2(int arr[][7]);//player2 turn
void CheckWin(int arr[][7], int &check); //check win condition

int main()
{
	int arr[6][7] = { 0 }; //initial table 6x7
	int play = 1; //default player first
	int check = 0; //check win if 0 that mean doesn't win
	int round = 0; //check round play
 
	cout << "This is connect 4 game !!" << endl;
	Table(); //show initial table
	
	cout << "Player1 first [1] , Player2 first [2] : "; //player select turn
	cin >> play;

	while (play > 2 || play < 1)
	{
		cout << "Incorrect input, Try again! : ";
		cin >> play;
	}

	while (check==0 && round <= 42 )
	{
		if (play == 1)  //if player 
		{
			player(arr); //player play
			UpdateTable(arr);
			play = 2;
		}
		else if (play == 2)  //if bot 
		{
			player2(arr); //player2 play
			UpdateTable(arr);
			play = 1;
		}
		CheckWin(arr, check); //check win
		round++; //round increase
	}
	if (round == 42) //if round = 42 that mean table full -> Draw
	{
		cout << "\nDraw...!!" << endl;
	}
	system("PAUSE");
	return 0;
}


//show initial table
void Table()
{
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "  |   |   |   |   |   |   |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << "1 | 2 | 3 | 4 | 5 | 6 | 7 |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
	cout << endl;
}

//Update table
void UpdateTable(int arr[][7])
{
	cout << "+++++++++++++++++++++++++++" << endl;
	for (int i = 5; i >= 0; i--)
	{
		for (int j = 0; j < 7; j++)
		{ 
			if (arr[i][j] == 0) //if 0 that mean place empty
			{
				cout << " ";
			}
			else if (arr[i][j] == 1) //if 1 that mean player
			{
				cout << "X";
			}
			else if (arr[i][j] == 2) //if 2 that mean bot
			{
				cout << "O";
			}
			if (j < 7)
			{
				cout << " | ";
			}
		}
		cout << endl;
		if (i < 6)
		{
			cout << "+++++++++++++++++++++++++++" << endl;
		}
	}
	cout << "1 | 2 | 3 | 4 | 5 | 6 | 7 |" << endl;
	cout << "+++++++++++++++++++++++++++" << endl;
}

//player turn
void player(int arr[][7])
{
	int player = 1;
	int row = 0;
	int col;
	int check = 0;

	//prevent incorrect input
	do {
		check = 0;
		cout << "Player1 turn select column [1-7] : ";
		cin >> col;
		while (col > 7 || col < 1)
		{
			cout << "Incorrect input, Try again! : ";
			cin >> col;
		}
		if (arr[5][col - 1] != 0)
		{
			check = 1;
		}
	} while (check == 1);

	//find row
	while (arr[row][col - 1] != 0)
	{
		row++;
	}
	//update table
	arr[row][col - 1] = player;
}

//player2 turn
void player2(int arr[][7])
{
	int player2 = 2;
	int row = 0;
	int col;
	int check = 0;

	//prevent incorrect input
	do {
		check = 0;
		cout << "Player2 turn select column [1-7] : ";
		cin >> col;
		while (col > 7 || col < 1)
		{
			cout << "Incorrect input, Try again! : ";
			cin >> col;
		}
		if (arr[5][col - 1] != 0)
		{
			check = 1;
		}
	} while (check == 1);

	//find row
	while (arr[row][col - 1] != 0)
	{
		row++;
	}
	//update table
	arr[row][col - 1] = player2;
}

//check win condition
void CheckWin(int arr[][7], int &check)
{
	//win check diagnal (Down)
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr[i + 1][j + 1] == 1 && arr[i + 2][j + 2] == 1 && arr[i + 3][j + 3] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i + 1][j + 1] == 2 && arr[i + 2][j + 2] == 2 && arr[i + 3][j + 3] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}

	//win check diagnal (Up)
	for (int i = 5; i >= 3; i--)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr[i - 1][j + 1] == 1 && arr[i - 2][j + 2] == 1 && arr[i - 3][j + 3] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i - 1][j + 1] == 2 && arr[i - 2][j + 2] == 2 && arr[i - 3][j + 3] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}
	//win check vertical
	for (int j = 0; j < 7; j++)
	{
		for (int i = 0; i < 3; i++)
		{
			if (arr[i + 1][j] == 1 && arr[i + 2][j] == 1 && arr[i + 3][j] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i + 1][j] == 2 && arr[i + 2][j] == 2 && arr[i + 3][j] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}
	//win check horizontal
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr[i][j + 1] == 1 && arr[i][j + 2] == 1 && arr[i][j + 3] == 1 && arr[i][j] == 1)
			{
				check = 1;
			}
			else if (arr[i][j + 1] == 2 && arr[i][j + 2] == 2 && arr[i][j + 3] == 2 && arr[i][j] == 2)
			{
				check = 2;
			}
		}
	}

	if (check == 1)//if check = 1 that mean player win
	{
		cout << "\nPlayer1 win...!" << endl;
	}
	else if (check == 2) //if ceck = 2 that mean bot win
	{
		cout << "\nPlayer2 Win...!" << endl;
	}
}
