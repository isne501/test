#include <iostream>
#include <string>
using namespace std;

int main()
{
	string message;
	
	cout << "Input your message : ";
	getline(cin,message); //Bring each charactor each new line
 
	int check = 0, charsize = message.length()-1;
    
	for(int i = 0 , j = charsize ; i < j ; i++ , j-- ) //Compare with first charactor and last charactor if not same charactor break loop , else continue until loop count eaual to charsize
	{
		if(message.at(i)!=message.at(j)) //mean charactor at first not same at last charactor
		{
			check = 0;
			break ; //stop loop if charactor at first not same at last charactor is true
		}
		else
			check = 1;
	}
	if(check==0)
		cout << "Your message aren't palindrome! \n"; //show to result of palindrome 
	else
		cout << "Your message are palindrome ! \n" ;
return 0;
}
